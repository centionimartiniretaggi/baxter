import os
import sys
import glob
import shutil
import datetime
from config import *


#EXTRACT-FEATURES:
neg_type = ["sample","other"]
#neg_type = [neg_type]
pixels_per_cell = [[8, 8], [6, 6], [4, 4]]
#pixels_per_cell = [pixels_per_cell]
cells_per_block = [[4, 4], [3, 3], [2, 2]]
#cells_per_block = [cells_per_block]
block_norm = ["L1", "L2", "L1-sqrt", "L2-Hys"]
#block_norm = [block_norm]
transform_sqrt = [True, False]
#transform_sqrt = [transform_sqrt]
#TRAIN

#TEST:
min_wdw_sz = [[100, 100]]
#min_wdw_sz = [min_wdw_sz]
step_size = [[10, 10], [20, 20], [50, 50]]
#step_size = [step_size]
downscale =[1.10, 1.25 , 1.5, 2]
#downscale = [downscale]
#test_im_path = 9 snapshot
threshold = [.1, .2, .3 , .5,]
#threshold = [threshold]


obj = raw_input('Insert the object to detect: ')
uobj = obj.title()
pos_path = "../data/dataset/{}Data/pos/".format(uobj)
neg_path = "../data/dataset/{}Data/neg/".format(uobj)



time_now=datetime.datetime.now()

for neg_t in neg_type:
    for ppc in pixels_per_cell:
        for cpb in cells_per_block:
            for bk in block_norm:
                for ts in transform_sqrt:
                    checkSqrt=""
                    if ts==True:
                        checkSqrt=" -ts"

                    # Extract the features
                    print "Inizio extract"
                    os.system("python ./extract-features.py -p {} -n {} -ppc {} {} -cpb {} {} -bk {} -obj {} -neg_t {}".format(pos_path, neg_path, ppc[0], ppc[1], cpb[0], cpb[1], bk, uobj, neg_t)+checkSqrt)
                    
                    # Perform training
                    print "Inizio train"
                    os.system("python ./train-classifier.py")
                    

                    for tip in glob.glob(os.path.join(test_im_path, "*")):
                        name = os.path.split(tip)[1].split(".")[0]
                        for d in downscale:
                            for ss in step_size:
                                for mws in min_wdw_sz:
                                    #creo il nome per i risultati
                                    result = "{}-{}-{}-{}-{}-{}-{}-{}-{}".format(name,ppc,cpb,bk,ts,d,ss,mws,neg_t)
                                    print result
                                    #creo il parametro per passare il vettore di threshold
                                    par_th = " -th"
                                    for th in threshold:
                                        par_th=par_th+" {}".format(th)
                                    # Perform testing #all'interno della funzione devo salvare l'immagine con tutti le detection e un immagine per ogni threshold
                                    os.system("python ./test-classifier.py -i {} -d {} -ss {} {} -mws {} {} -res '{}' -ppc {} {} -cpb {} {} -bk {}".format(tip, d, ss[0], ss[1], mws[0], mws[1], result, ppc[0], ppc[1], cpb[0], cpb[1], bk)+par_th+checkSqrt)

                    #ora devo cancellare le cartelle data/features e data/models
                    shutil.rmtree(os.path.split(pos_feat_path)[0])
                    shutil.rmtree(os.path.split(model_path)[0])

print "INIZIO: {}".format(time_now)
print "FINE: {}".format(datetime.datetime.now())
raw_input("Press Enter to finish...")
