#!/usr/bin/python

# Import the required modules
from skimage.feature import local_binary_pattern
from sklearn.svm import LinearSVC
from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib
import argparse as ap
import glob
import os
from config import *
import numpy as np

if __name__ == "__main__":
    # Parse the command line arguments
    parser = ap.ArgumentParser()
    parser.add_argument('-c', "--classifier", help="Classifier to be used", default="LIN_SVM")
    parser.add_argument('-cont', "--contatore", help="contatore", default="0", type=str)
    args = vars(parser.parse_args())

    # Classifiers supported
    clf_type = args['classifier']
    cont = args["contatore"]
    if cont!="0":
        model_path = "../data/models/svm{}.model".format(cont)
        pos_feat_path = "../data/features/pos{}".format(cont)
        neg_feat_path = "../data/features/neg{}".format(cont)
    
    fds = []
    labels = []

    # Load the positive features
    for feat_path in glob.glob(os.path.join(pos_feat_path,"*.feat")):        
        fd = joblib.load(feat_path)
        length_array_positives = len(fd)

        fds.append(fd)
        labels.append(1)
    print "Thread {} - Positive features loaded.".format(cont)

    # Load the negative features
    for feat_path in glob.glob(os.path.join(neg_feat_path,"*.feat")):
        fd = joblib.load(feat_path)
        length_array_negatives = len(fd)
      
        fds.append(fd)
        labels.append(0)
    print "Thread {} - Negative features loaded.".format(cont)



    clf = LinearSVC()
    #print "Training a Linear SVM Classifier"
    clf.fit(fds, labels)
    # If feature directories don't exist, create them
    if not os.path.isdir(os.path.split(model_path)[0]):
        os.makedirs(os.path.split(model_path)[0])
    joblib.dump(clf, model_path)
    #print "Classifier saved to {}".format(model_path)
