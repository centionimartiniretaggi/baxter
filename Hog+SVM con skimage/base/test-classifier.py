#!/usr/bin/python

# Import the required modules
import os
import argparse as ap
from skimage.transform import pyramid_gaussian
from skimage.io import imread
from skimage.feature import hog
from sklearn.externals import joblib
import cv2
import imutils 
import numpy as np

from nms import nms
from config import *

def pyramid(image, scale, minSize):
    # yield the original image
    yield image
    # keep looping over the pyramid
    while True:
		# compute the new dimensions of the image and resize it
        wi = int(image.shape[1] / scale)
        image = imutils.resize(image, width=wi)

		# if the resized image does not meet the supplied minimum
		# size, then stop constructing the pyramid
        if image.shape[0] < minSize[1] or image.shape[1] < minSize[0]:
            break

		# yield the next image in the pyramid
        yield image

#Malisiewicz et al.
def non_max_suppression_fast(boxes, overlapThresh):
    boxes = np.array(boxes)
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []
	# if the bounding boxes integers, convert them to floats --
	# this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

	# initialize the list of picked indexes
    pick = []

    
	# grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 0] + boxes[:, 3]
    y2 = boxes[:, 1] + boxes[:, 4]

	# compute the area of the bounding boxes and sort the bounding
	# boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)

	# keep looping while some indexes still remain in the indexes
	# list
    while len(idxs) > 0:
		# grab the last index in the indexes list and add the
		# index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)

		# find the largest (x, y) coordinates for the start of
		# the bounding box and the smallest (x, y) coordinates
		# for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])

		# compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

		# compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]

		# delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last], np.where(overlap > overlapThresh)[0])))

	# return only the bounding boxes that were picked using the
	# integer data type
    return boxes[pick].astype("int")

def sliding_window(image, window_size, step_size):
    '''
    This function returns a patch of the input image `image` of size equal
    to `window_size`. The first image returned top-left co-ordinates (0, 0) 
    and are increment in both x and y directions by the `step_size` supplied.
    So, the input parameters are -
    * `image` - Input Image
    * `window_size` - Size of Sliding Window
    * `step_size` - Incremented Size of Window

    The function returns a tuple -
    (x, y, im_window)
    where
    * x is the top-left x co-ordinate
    * y is the top-left y co-ordinate
    * im_window is the sliding window image
    '''
    for y in xrange(0, image.shape[0], step_size[1]):
        for x in xrange(0, image.shape[1], step_size[0]):
            yield (x, y, image[y:y + window_size[1], x:x + window_size[0]])

if __name__ == "__main__":
    # Parse the command line arguments
    parser = ap.ArgumentParser()
    parser.add_argument('-i', "--image", help="Path to the test image", required=True)
    parser.add_argument('-d', '--downscale', help="Downscale ratio", default=downscale, type=float)
    parser.add_argument('-ss', '--step_size', help="step_size", default=step_size, nargs='+', type=int)
    parser.add_argument('-mws', '--min_wdw_sz', help="min_wdw_sz", default=min_wdw_sz, nargs='+', type=int)
    parser.add_argument('-res', '--result', help="result", default="", type=str)
    parser.add_argument('-v', '--visualize', help="Visualize the sliding window", action="store_true")
    parser.add_argument('-w', '--wait', help="Wait for the result", action="store_true")
    parser.add_argument('-th', '--threshold', help="threshold", default=[threshold], nargs='+', type=float)
    parser.add_argument('-ppc', "--pixels_per_cell", help="pixels_per_cell", nargs='+', type=int, default=pixels_per_cell)
    parser.add_argument('-cpb', "--cells_per_block", help="cells_per_block", nargs='+', type=int, default=cells_per_block)
    parser.add_argument('-bk', "--block_norm", help="block_norm", default=block_norm)
    parser.add_argument('-ts', '--transform_sqrt', help="transform_sqrt", action="store_true")
    parser.add_argument('-cont', "--contatore", help="contatore", default="0", type=str)
    
    args = vars(parser.parse_args())

    # Read the image
    im = imread(args["image"], as_grey=False)

    result = args['result']
    if result=="":
        result_name = os.path.split(args["image"])[1].split(".")[0]
        result=result_path+"/"+result_name
    else:
        result=result_path+"/"+result
        
    d = args['downscale']
    visualize_det = args['visualize']
    wait = args['wait']
    bk = args["block_norm"]
    ts = args["transform_sqrt"]
    cont = args["contatore"]
    if cont!="0":
        model_path = "../data/models/svm{}.model".format(cont)

    for name, value in parser.parse_args()._get_kwargs():
        if name == "step_size":
            if value is not None:
                ss = value
        if name == "min_wdw_sz":
            if value is not None:
                mws = value
        if name == "threshold":
            if value is not None:
                threshold = value
        if name == "pixels_per_cell":
            if value is not None:
                ppc = value
        if name == "cells_per_block":
            if value is not None:
                cpb = value

    # Load the classifier
    clf = joblib.load(model_path)

    # List to store the detections
    detections = []
    # The current scale of the image
    scale = 0
    # Downscale the image and iterate
    #for im_scaled in pyramid_gaussian(im, downscale=d):  Image Pyramid di Skimage
    for im_scaled in pyramid(im, d, mws):
        #print "Scale: {}".format(scale)
        # This list contains detections at the current scale
        cd = []
        # If the width or height of the scaled image is less than
        # the width or height of the window, then end the iterations.
        if im_scaled.shape[0] < mws[1] or im_scaled.shape[1] < mws[0]:
            break
        for (x, y, im_window) in sliding_window(im_scaled, mws, ss):
            if im_window.shape[0] != mws[1] or im_window.shape[1] != mws[0]:
                continue
            # Calculate the HOG features
            fd = hog(im_window, orientations=orientations, pixels_per_cell=ppc, cells_per_block=cpb, block_norm=bk, visualise=False, transform_sqrt=ts)
            fd = fd.reshape(1, -1)
            pred = clf.predict(fd)
            if pred == 1:
                #print  "Detection:: Location -> ({}, {}) ({}, {})".format(x, y, int(mws[0]*(d**scale)), int(mws[1]*(d**scale)))
                #print "Scale ->  {} | Confidence Score {} \n".format(scale, clf.decision_function(fd))
                detections.append((x, y, clf.decision_function(fd), int(mws[0]*(d**scale)), int(mws[1]*(d**scale)), int(x*(d**scale)), int(y*(d**scale))))
                cd.append(detections[-1])
            # If visualize is set to true, display the working
            # of the sliding window
            if visualize_det:
                clone = im_scaled.copy()
                for x1, y1, _, _, _, _, _  in cd:
                    # Draw the detections at this scale
                    cv2.rectangle(clone, (x1, y1), (x1 + im_window.shape[1], y1 + im_window.shape[0]), (0, 0, 0), thickness=2)
	            #img_color = cv2.cvtColor(clone,cv2.COLOR_GRAY2RGB)
                cv2.rectangle(clone, (x, y), (x + im_window.shape[1], y + im_window.shape[0]), (255, 255, 255), thickness=2)
                cv2.imshow("Sliding Window in Progress", clone)
                if wait:
                    cv2.waitKey(30)
        # Move the the next scale
        #print "   {} Detections Founded".format(len(cd))
        scale += 1

    #Se non esiste, creo la cartella per salvare i risultati
    if not os.path.isdir(result_path):
        os.makedirs(result_path)

    # Display the results before performing NMS
    clone = im.copy()
    #for (x_tl, y_tl, _, w, h, x_mod, y_mod) in detections:
        # Draw the detections
        #print "x_tl: {}".format(x_tl)
        #print "y_tl: {}".format(y_tl)
        #print "w: {}".format(w)
        #print "h: {}".format(h)
    #    cv2.rectangle(im, (x_mod, y_mod), (x_mod+w, y_mod+h), (0, 0, 0), thickness=2)
    #if visualize_det:
    #    cv2.imshow("Raw Detections before NMS", im)
    #cv2.imwrite(result+'-original.png',im)
    #if wait:
    #    cv2.waitKey()


    for th in threshold: 
        #print "Perfoming NMS with threshold={}".format(th)
        th_clone = clone.copy()
        # Perform Non Maxima Suppression
        th_detections = non_max_suppression_fast(detections, th)

        th_clone2 = clone.copy()
        # Perform Non Maxima Suppression
        th_detections2 = nms(detections, th)


        if len(th_detections)<10:
            # Display the results after performing NMS_Fast
            for (x_tl, y_tl, decision, w, h, x_mod, y_mod) in th_detections:
                # Draw the detections
                cv2.rectangle(th_clone, (x_mod, y_mod), (x_mod+w,y_mod+h), (0, 0, 0), thickness=2)
            if visualize_det:
                cv2.imshow("Final Detections after applying NMS_Fast with threshold={}".format(th), th_clone)
            cv2.imwrite(result+'-{}-fast.png'.format(th),th_clone)
            if wait:
                cv2.waitKey()
        
        #dec = 0
        #ind = 0
        #cont = 0
        if len(th_detections2)<10:
            # Display the results after performing NMS
            for (x_tl, y_tl, decision, w, h, x_mod, y_mod) in th_detections2:
                # Draw the detections
                cv2.rectangle(th_clone2, (x_mod, y_mod), (x_mod+w,y_mod+h), (0, 0, 0), thickness=2)
                #if decision>dec:
                #    dec = decision
                #    ind = cont
                #cont+=1
            #det = th_detections2[ind]
            #cv2.rectangle(th_clone2, (det[5], det[6]), (det[5]+det[3],det[6]+det[4]), (255, 0, 0), thickness=2)
            if visualize_det:
                cv2.imshow("Final Detections after applying NMS with threshold={}".format(th), th_clone2)
            cv2.imwrite(result+'-{}.png'.format(th),th_clone2)
            if wait:
                cv2.waitKey()
    #print "Test Completed!"
