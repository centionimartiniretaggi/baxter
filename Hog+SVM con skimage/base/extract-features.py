#!/usr/bin/python

# Import the functions to calculate feature descriptors
from skimage.feature import local_binary_pattern
from skimage.feature import *
from skimage.io import imread
from sklearn.externals import joblib
# To read file names
import argparse as ap
import glob
import os
from config import *


#funzione che utilizza come negativi immagini random
def negative_samples():
    #print "Calculating the descriptors for the negative samples and saving them"
    for im_path in glob.glob(os.path.join(neg_im_path, "*")):
        im = imread(im_path, as_grey=False)
        if des_type == "HOG":
            fd = hog(im, orientations=orientations, pixels_per_cell=ppc, cells_per_block=cpb, block_norm=bk, visualise=False, transform_sqrt=ts)
        fd_name = os.path.split(im_path)[1].split(".")[0] + ".feat"
        fd_path = os.path.join(neg_feat_path, fd_name)
        joblib.dump(fd, fd_path)
    #print "Negative features saved in {}".format(neg_feat_path)

#funzione che utilizza come negativi tutti gli altri oggetti
def negative_other():
    vect = ["Banana", "Orange", "Book", "Box", "Key"]
    for v in vect:
        if v != obj:
            i=0
            pos_path_other = "../data/dataset/{}Data/pos/".format(v)

            #print "Calculating the descriptors for the negative objects"
            for im_path in glob.glob(os.path.join(pos_path_other, "*")):
                if i>500:
                    break
                im = imread(im_path, as_grey=False)
                if des_type == "HOG":
                    fd = hog(im, orientations=orientations, pixels_per_cell=ppc, cells_per_block=cpb, block_norm=bk, visualise=False, transform_sqrt=ts)
                fd_name = os.path.split(im_path)[1].split(".")[0] + "{}.feat".format(v)
                fd_path = os.path.join(neg_feat_path, fd_name)
                joblib.dump(fd, fd_path)
                i+=1

if __name__ == "__main__":
    # Argument Parser
    parser = ap.ArgumentParser()
    parser.add_argument('-p', "--pospath", help="Path to positive images", required=True)
    parser.add_argument('-n', "--negpath", help="Path to negative images", required=True)
    parser.add_argument('-obj', "--object", help="Selected Object", required=True)
    parser.add_argument('-d', "--descriptor", help="Descriptor to be used -- HOG", default="HOG")
    parser.add_argument('-ppc', "--pixels_per_cell", help="pixels_per_cell", nargs='+', type=int, default=pixels_per_cell)
    parser.add_argument('-cpb', "--cells_per_block", help="cells_per_block", nargs='+', type=int, default=cells_per_block)
    parser.add_argument('-bk', "--block_norm", help="block_norm", default=block_norm)
    parser.add_argument('-ts', '--transform_sqrt', help="transform_sqrt", action="store_true")
    parser.add_argument('-neg_t', "--neg_type", help="How to select negatives", default=neg_type)
    parser.add_argument('-cont', "--contatore", help="contatore", default="0", type=str)

    args = vars(parser.parse_args())

    pos_im_path = args["pospath"]
    neg_im_path = args["negpath"]
    obj= args["object"]
    des_type = args["descriptor"]
    bk = args["block_norm"]
    ts = args["transform_sqrt"]
    neg_t = args["neg_type"]
    cont = args["contatore"]
    if cont!="0":
        pos_feat_path = "../data/features/pos{}".format(cont)
        neg_feat_path = "../data/features/neg{}".format(cont)

    for name, value in parser.parse_args()._get_kwargs():
        if name == "pixels_per_cell":
            if value is not None:
                ppc = value
        if name == "cells_per_block":
            if value is not None:
                cpb = value
    # If feature directories don't exist, create them
    if not os.path.isdir(pos_feat_path):
        os.makedirs(pos_feat_path)

    # If feature directories don't exist, create them
    if not os.path.isdir(neg_feat_path):
        os.makedirs(neg_feat_path)

    #print "Calculating the descriptors for the positive samples and saving them"
    i=0
    for im_path in glob.glob(os.path.join(pos_im_path, "*")):
        if i>1000:
            break
        im = imread(im_path, as_grey=False)
        if des_type == "HOG":
            fd = hog(im, orientations=orientations, pixels_per_cell=ppc, cells_per_block=cpb, block_norm=bk, visualise=False, transform_sqrt=ts)
        fd_name = os.path.split(im_path)[1].split(".")[0] + ".feat"
        fd_path = os.path.join(pos_feat_path, fd_name)
        joblib.dump(fd, fd_path)
        i+=1
    #print "Positive features saved in {}".format(pos_feat_path)


    if neg_t == "sample":
        #Utilizzo come negativi delle immagini random
        negative_samples()
    elif neg_t == "other":
        #Utilizzo come negativi tutti gli altri oggetti
        negative_other()



    #print "Completed calculating features from training images"
