import cv2
import math
import os
import numpy as np
import shutil
import glob

def mod_and_save(img,path,cont):
    cv2.imwrite(path+'_resized_' +str(cont)+'.jpg',img)
    #Blur --> Smoothing
    blur_image = cv2.blur(img, (5,5))
    #cv2.imwrite(path+'_resized_blur_'+str(cont)+'.jpg', blur_image)
    #Blur --> Median
    blurmedian_image = cv2.medianBlur(img, 5)
    #cv2.imwrite(path+'_resized_blurmedian_'+str(cont)+'.jpg', blurmedian_image)
    #Blur --> Bilateral Filters
    bilateralfilter_image = cv2.bilateralFilter(img,9,75,75)
    #cv2.imwrite(path+'_resized_bilateralfilter_'+str(cont)+'.jpg', bilateralfilter_image)
    #Brightness +0.5
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV) #convert it to hsv        
    h, s, v = cv2.split(hsv)
    v=np.where((255-v)<50,255,v+50)
    final_hsv = cv2.merge((h, s, v))
    brightness_plus_img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    cv2.imwrite(path+'_resized_brightnessplus_' +str(cont)+'.jpg',brightness_plus_img)
    #Brightness -0.5    
    h, s, v = cv2.split(hsv)
    v=np.where(v>50,v-50,0)
    final_hsv = cv2.merge((h, s, v))
    brightness_minus_img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    cv2.imwrite(path+'_resized_brightnessminus_' +str(cont)+'.jpg',brightness_minus_img)

    # equalize the histogram of the Y channel
    img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
    img_yuv[:,:,0] = cv2.equalizeHist(img_yuv[:,:,0])
    img_output = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)
    #cv2.imwrite(path+'_resized_equalized_' +str(cont)+'.jpg',img_output)

    #-----Applying CLAHE to L-channel-------------------------------------------
    lab= cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)
    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
    cl = clahe.apply(l)
    limg = cv2.merge((cl,a,b))
    final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)
    #cv2.imwrite(path+'_resized_equalized_clake_' +str(cont)+'.jpg',final)

    #terzo contrasto
    imgCont=img.copy()
    cv2.convertScaleAbs(img,imgCont,1.5)
    #cv2.imwrite(path+'_resized_contrast_' +str(cont)+'.jpg',imgCont)
    #quarto contrasto
    imgCont2=img.copy()
    cv2.convertScaleAbs(img,imgCont2,2)
    #cv2.imwrite(path+'_resized_contrast2_' +str(cont)+'.jpg',imgCont2)



def tagli(img,x,y,w,h,resize,path):
    crop=[]
    cont=1
    crop.append(img[y:(y+h), x:(x+w)])
    pixels=5 #pixel da aggiungere al crop ad ogni ciclo
    cicli=20/pixels +1
    for i in range(1,cicli):   #2pixel * 10 volte = 20 pixel totali dal crop iniziale
        add=i*pixels
        ww=w+add
        hh=h+add
        if resize==True:
            for xx in range(x-add,x,pixels):
                for yy in range(y-add,y,pixels):
                    #clone=img.copy()
                    #cv2.rectangle(clone, (xx, yy), (xx + ww, yy + hh), (255, 0, 0), thickness=2)
                    #cv2.rectangle(clone, (x, y), (x + w, y + h), (0, 0, 255), thickness=2)
                    #cv2.imshow("prova2", clone)
                    #cv2.waitKey()

                    crop.append(img[yy:(yy+hh), xx:(xx+ww)])
        else:
            crop.append(img[y-add:(y+h+add), x-add:(x+w+add)])

    for c in crop:
        if resize==True:
            resized = cv2.resize(c, (100, 100))
        else:
            resized=c
        #mod_and_save(resized,path,cont)
        cont+=1



def init(obj,frame,x,y,w,h):
    path0="new/{}Data/pos".format(obj)
    path1="new/{}Data/neg".format(obj)
    path2="new/{}Data/negOr".format(obj)
    if not os.path.isdir(path0):
        os.makedirs(path0)
    if not os.path.isdir(path1):
        os.makedirs(path1)
    if not os.path.isdir(path2):
        os.makedirs(path2)
    
    img = cv2.imread("./{}/{}.jpg".format(obj,frame),cv2.IMREAD_COLOR)
    clone =img.copy()
    cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), thickness=2)
    cv2.rectangle(img, (x-20, y-20), (x + w+20, y + h+20), (0, 255, 0), thickness=2)
    cv2.imshow("prova", img)
    cv2.waitKey()


    tagli(clone,x,y,w,h,True,path0+"/"+frame)
    tagli(clone,x,y,w,h,True,path1+"/"+frame)
    tagli(clone,x,y,w,h,False,path2+"/"+frame)




group=1

if group==1:
    #GRUPPO 1
    init("Banana","frame0061",530,280,210,210)
    init("Banana","frame0065",530,280,210,210)
    init("Pocket","frame0033",580,290,130,130)
    init("Pocket","frame0038",575,285,130,130)
    init("Box","frame0089",535,270,220,220)
        #init("Box","frame0091",535,270,220,220)
    init("Box","frame0094",535,280,220,220)
    init("Orange","frame0012",588,340,105,105)
        #init("Orange","frame0017",588,340,105,105)
    init("Orange","frame0024",597,333,105,105)
else:
    #GRUPPO 2
    init("Book","frame0005",510,272,270,270)
    init("Book","frame0007",508,263,270,270)
        #init("Book","frame0010",508,263,270,270)
    #init("Bottle","frame0042",470,220,313,313)     #FRONT
    #init("Bottle","frame0049",470,220,313,313)     #FRONT
    init("Bottle","frame0054",552,240,192,192)      #TOP
    #init("Glasses","frame0071",534,287,160,160)    #CLOSE
    #init("Glasses","frame0074",534,287,160,160)    #CLOSE
    init("Glasses","frame0078",534,322,190,190)     #OPEN
    init("Key","frame0025",510,282,222,222)
    init("Key","frame0029",510,282,224,224)
