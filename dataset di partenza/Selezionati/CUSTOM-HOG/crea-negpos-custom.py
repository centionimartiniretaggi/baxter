import cv2
import math
import os
import numpy as np
import shutil
import glob

def mod_and_save(img,path,cont):
    cv2.imwrite(path+'_resized_' +str(cont)+'.jpg',img)
    #Blur --> Smoothing
    blur_image = cv2.blur(img, (5,5))
    cv2.imwrite(path+'_resized_blur_'+str(cont)+'.jpg', blur_image)
    #Blur --> Median
    blurmedian_image = cv2.medianBlur(img, 5)
    cv2.imwrite(path+'_resized_blurmedian_'+str(cont)+'.jpg', blurmedian_image)
    #Blur --> Bilateral Filter
    bilateralfilter_image = cv2.bilateralFilter(img,9,75,75)
    cv2.imwrite(path+'_resized_bilateralfilter_'+str(cont)+'.jpg', bilateralfilter_image)
    #Brightness +0.5
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV) #convert it to hsv        
    h, s, v = cv2.split(hsv)
    v=np.where((255-v)<50,255,v+50)
    final_hsv = cv2.merge((h, s, v))
    brightness_plus_img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    cv2.imwrite(path+'_resized_brightnessplus_' +str(cont)+'.jpg',brightness_plus_img)
    #Brightness -0.5    
    h, s, v = cv2.split(hsv)
    v=np.where(v>50,v-50,0)
    final_hsv = cv2.merge((h, s, v))
    brightness_minus_img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    cv2.imwrite(path+'_resized_brightnessminus_' +str(cont)+'.jpg',brightness_minus_img)

    # equalize the histogram of the Y channel
    img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
    img_yuv[:,:,0] = cv2.equalizeHist(img_yuv[:,:,0])
    img_output = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)
    #cv2.imwrite(path+'_resized_equalized_' +str(cont)+'.jpg',img_output)

    #-----Applying CLAHE to L-channel-------------------------------------------
    lab= cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)
    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
    cl = clahe.apply(l)
    limg = cv2.merge((cl,a,b))
    final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)
    #cv2.imwrite(path+'_resized_equalized_clake_' +str(cont)+'.jpg',final)

    #terzo contrasto
    imgCont=img.copy()
    cv2.convertScaleAbs(img,imgCont,1.5)
    #cv2.imwrite(path+'_resized_contrast_' +str(cont)+'.jpg',imgCont)
    #quarto contrasto
    imgCont2=img.copy()
    cv2.convertScaleAbs(img,imgCont2,2)
    #cv2.imwrite(path+'_resized_contrast2_' +str(cont)+'.jpg',imgCont2)

def crops(img,path,name,resize):
    path1=path+"/"+name
    crop=[]
    cont=1
    crop.append(img)
    (h, w) = img.shape[:2]

    #cv2.rectangle(img, (20, 20), (w-20, h-20), (255, 0, 0), thickness=2)
    #cv2.imshow("prova", img)
    #cv2.waitKey()
    #cv2.rectangle(img, (40, 40), (w-40, h-40), (255, 0, 0), thickness=2)
    #cv2.imshow("prova", img)
    #cv2.waitKey()
    #cv2.rectangle(img, (60, 60), (w-60, h-60), (255, 0, 0), thickness=2)
    #cv2.imshow("prova", img)
    #cv2.waitKey()
    
    
    crop.append(img[20:(h-20), 20:(w-20)])
    crop.append(img[40:(h-40), 40:(w-40)])
    crop.append(img[60:(h-60), 60:(w-60)])

    crop.append(img[20:(h-60), 20:(w-60)])
    crop.append(img[20:(h-60), 40:(w-40)])
    crop.append(img[20:(h-60), 60:(w-20)])
    crop.append(img[40:(h-40), 20:(w-60)])
    crop.append(img[40:(h-40), 40:(w-40)])
    crop.append(img[40:(h-40), 60:(w-20)])
    crop.append(img[60:(h-20), 20:(w-60)])
    crop.append(img[60:(h-20), 40:(w-40)])
    crop.append(img[60:(h-20), 60:(w-20)])

    for c in crop:
        if resize==True:
            resized = cv2.resize(c, (100, 100))
        else:
            resized=c 
        mod_and_save(resized,path1,cont)
        cont+=1

    #cv2.rectangle(img, (20, 20), (w-60, h-60), (0, 255, 0), thickness=2)
    #cv2.rectangle(img, (40, 20), (w-40, h-60), (0, 255, 0), thickness=2)
    #cv2.rectangle(img, (60, 20), (w-20, h-60), (0, 255, 0), thickness=2)
    #cv2.rectangle(img, (20, 40), (w-60, h-40), (0, 255, 0), thickness=2)
    #cv2.rectangle(img, (40, 40), (w-40, h-40), (0, 255, 0), thickness=2)
    #cv2.rectangle(img, (60, 40), (w-20, h-40), (0, 255, 0), thickness=2)
    #cv2.rectangle(img, (20, 60), (w-60, h-20), (0, 255, 0), thickness=2)
    #cv2.rectangle(img, (40, 60), (w-40, h-20), (0, 255, 0), thickness=2)
    #cv2.rectangle(img, (60, 60), (w-20, h-20), (0, 255, 0), thickness=2)
    #cv2.imshow("prova", img)
    #cv2.waitKey()

def no_crops(img,path,name,resize):
    path1=path+"/"+name


    if resize==True:
        resized = cv2.resize(img, (100, 100))
    else:
        resized=img
    mod_and_save(resized,path1,1)
    

def init(obj,frame,x,y,w,h):
    path1="{}/pos".format(obj)
    path2="{}/posOr".format(obj)
    if not os.path.isdir(path1):
        os.makedirs(path1)
    if not os.path.isdir(path2):
        os.makedirs(path2)
    
    img = cv2.imread("./{}/{}.jpg".format(obj,frame),cv2.IMREAD_COLOR)
    clone =img.copy()
    cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), thickness=2)
    cv2.rectangle(img, (x+60, y+60), (x + w-60, y + h-60), (0, 255, 0), thickness=2)
    cv2.imshow("prova", img)
    cv2.waitKey()
    #creo 78 tipi diversi ---> 13 crops per 6 tipi di immagini
    crop_img = clone[y:(y+h), x:(x+w)]
    
    crops(crop_img,path1,frame,True)
    crops(crop_img,path2,frame,False)

def internet(type,resize):
    if resize==True:
        path_save="{}/internet".format(type)
    else:
        path_save="{}/internetOr".format(type)
    if not os.path.isdir(path_save):
            os.makedirs(path_save) 
    cont=1
    for im_path in glob.glob(os.path.join("./Internet/{}-resized".format(type), "*")):
        print im_path
        img = cv2.imread(im_path,cv2.IMREAD_COLOR)
        if type=="Banana":
            resized = cv2.resize(img, (300, 300))
        elif type=="Orange":
            resized = cv2.resize(img, (200, 200))
        
        (h, w) = resized.shape[:2]
        #cv2.rectangle(resized, (20, 20), (w-20, h-20), (255, 0, 0), thickness=2)
        #cv2.rectangle(resized, (40, 40), (w-40, h-40), (255, 0, 0), thickness=2)
        #cv2.rectangle(resized, (60, 60), (w-60, h-60), (255, 0, 0), thickness=2)
        #cv2.imshow("prova", resized)
        #cv2.waitKey()
        no_crops(resized,path_save,"{}{}".format(type,cont),resize)
        cont+=1



gruppo=2

vect=[]
if gruppo==1:
    vect=["Banana","Box","Orange","Pocket"]
else:
    vect=["Book","Bottle","Glasses","Key"]

path="new"
if not os.path.isdir(path):
    os.makedirs(path)

for type in vect:
    path1=path+"/"+type+"Data/pos/"
    path2=path+"/"+type+"Data/neg/"
    path3=path+"/"+type+"Data/negOr/"
    if not os.path.isdir(path1):
        os.makedirs(path1)
    if not os.path.isdir(path2):
        os.makedirs(path2)
    if not os.path.isdir(path3):
        os.makedirs(path3)
    cont=1
    for im_path in glob.glob(os.path.join("Gruppo{}/{}".format(gruppo,type), "*")):
        img = cv2.imread(im_path,cv2.IMREAD_COLOR)
        resized = cv2.resize(img, (100, 100))
        mod_and_save(resized,path1,cont)
        mod_and_save(resized,path2,cont)
        mod_and_save(img,path3,cont)
        cont+=1