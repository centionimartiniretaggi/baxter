#!/usr/bin/env python
from __future__ import division
import argparse
import struct
import sys
import os
import copy
import rospy
import rospkg
from testhogsvm import *

from gazebo_msgs.srv import (
    SpawnModel,
    DeleteModel,
)
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from std_msgs.msg import (
    Header,
    Empty,
)

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

import baxter_interface

import numpy
import cv2
import time
import math

from cv_bridge import CvBridge
import transformations
from baxter_interface import CameraController
from baxter_interface import Gripper
from sensor_msgs.msg import Image

"""
starting_joint_angles = {'left_w0': 0.6699952259595108,
                             'left_w1': 1.030009435085784,
                             'left_w2': -0.4999997247485215,
                             'left_e0': -1.189968899785275,
                             'left_e1': 1.9400238130755056,
                             'left_s0': -0.08000397926829805,
                             'left_s1': -0.9999781166910306}


# An orientation for gripper fingers to be overhead and parallel to the obj
overhead_orientation = Quaternion(
                             x=-0.0249590815779,
                             y=0.999649402929,
                             z=0.00737916180073,
                             w=0.00486450832011)
"""

class Moving(object):
    def __init__(self, limb, hover_distance = 0.15, verbose=True):
        self._limb_name = limb # string
        self._hover_distance = hover_distance # in meters
        self._verbose = verbose # bool
        self._limb = baxter_interface.Limb(limb)
        self._gripper = baxter_interface.Gripper(limb)
        self._gripper.calibrate()
        ns = "ExternalTools/" + limb + "/PositionKinematicsNode/IKService"
        self._iksvc = rospy.ServiceProxy(ns, SolvePositionIK)
        rospy.wait_for_service(ns, 5.0)
        # verify robot is enabled
        print("Getting robot state... ")
        self._rs = baxter_interface.RobotEnable(baxter_interface.CHECK_VERSION)
        self._init_state = self._rs.state().enabled
        print("Enabling robot... ")
        self._rs.enable()

    def move_to_start(self, start_angles=None):
        print("Moving the {0} arm to start pose...".format(self._limb_name))
        if not start_angles:
            start_angles = dict(zip(self._joint_names, [0]*7))
        self._guarded_move_to_joint_position(start_angles)
        self.gripper_open()
        rospy.sleep(1.0)
        print("Running. Ctrl-c to quit")

    def _guarded_move_to_joint_position(self, joint_angles):
        if joint_angles:
            self._limb.move_to_joint_positions(joint_angles)
        else:
            rospy.logerr("No Joint Angles provided for move_to_joint_positions. Staying put.")

    def gripper_open(self):
        self._gripper.open()
        rospy.sleep(1.0)

    def gripper_close(self):
        self._gripper.close()
        rospy.sleep(1.0)

    def _approach(self, pose):
        approach = copy.deepcopy(pose)
        # approach with a pose the hover-distance above the requested pose
        approach.position.z = approach.position.z + self._hover_distance
        joint_angles = self.ik_request(approach)
        self._guarded_move_to_joint_position(joint_angles)

    def _servo_to_pose(self, pose):
        # servo down to release
        joint_angles = self.ik_request(pose)
        self._guarded_move_to_joint_position(joint_angles)

    def ik_request(self, pose):
        hdr = Header(stamp=rospy.Time.now(), frame_id='base')
        ikreq = SolvePositionIKRequest()
        ikreq.pose_stamp.append(PoseStamped(header=hdr, pose=pose))
        try:
            resp = self._iksvc(ikreq)
        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))
            return False
        # Check if result valid, and type of seed ultimately used to get solution
        # convert rospy's string representation of uint8[]'s to int's
        resp_seeds = struct.unpack('<%dB' % len(resp.result_type), resp.result_type)
        limb_joints = {}
        if (resp_seeds[0] != resp.RESULT_INVALID):
            seed_str = {
                        ikreq.SEED_USER: 'User Provided Seed',
                        ikreq.SEED_CURRENT: 'Current Joint Angles',
                        ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
                       }.get(resp_seeds[0], 'None')
            if self._verbose:
                print("IK Solution SUCCESS - Valid Joint Solution Found from Seed Type: {0}".format(
                         (seed_str)))
            # Format solution into Limb API-compatible dictionary
            limb_joints = dict(zip(resp.joints[0].name, resp.joints[0].position))
            if self._verbose:
                print("IK Joint Solution:\n{0}".format(limb_joints))
                print("------------------")
        else:
            rospy.logerr("INVALID POSE - No Valid Joint Solution Found.")
            return False
        return limb_joints

    def _retract(self):
        # retrieve current pose from endpoint
        current_pose = self._limb.endpoint_pose()
        ik_pose = Pose()
        ik_pose.position.x = current_pose['position'].x 
        ik_pose.position.y = current_pose['position'].y 
        ik_pose.position.z = current_pose['position'].z + self._hover_distance
        ik_pose.orientation.x = current_pose['orientation'].x 
        ik_pose.orientation.y = current_pose['orientation'].y 
        ik_pose.orientation.z = current_pose['orientation'].z 
        ik_pose.orientation.w = current_pose['orientation'].w
        joint_angles = self.ik_request(ik_pose)
        # servo up from current pose
        self._guarded_move_to_joint_position(joint_angles)


def rotate(rad,limb):
    pnp = Moving(limb)
    current_pose = pnp._limb.endpoint_pose()['orientation']
    eul = transformations.euler_from_quaternion([current_pose.w, current_pose.x ,current_pose.y,current_pose.z]) 
    angles = [angle for angle in eul]
    angles[2] = angles[2] - rad
    quat = transformations.quaternion_from_euler(angles[0],angles[1],angles[2])
    orientation=Quaternion(quat[1],quat[2],quat[3],quat[0])
    return orientation

def main():
    #PARAMETERS:
    pathSnapshot='snapshot.png'
    pathResult = "result"
    pathCenterObject="result/center_point.png"
    pathFinalResult="result/final_result/center_point.png"
    height = 400 # resolution of the camera (height)
    width = 640 # resolution of the camera (width)
    calibration_factor = 0.0029
    distance = 0.30
    dist_gripper = 0.02 # more centimeters = less gripping

    #Folder for HOG+SVM results
    if not os.path.isdir(pathResult):
        os.makedirs(pathResult)
    

    #Untuck Command
    os.system("rosrun baxter_tools tuck_arms.py -u")
    
    mess=raw_input("Untuck Command in progress... \n Continue? (y/n) ")
    if mess=="n":
        sys.exit(0)
    #Select Baxter's arm
    mess=raw_input("Select Baxter's arm (l=left or r=right):  ")
    if mess=="r":
        limb = 'right'
        cam_controller = 'right_hand_camera'
        hand_range = 'right_hand_range'
        cam_x_offset = -0.020
        cam_y_offset = -0.025
        #top-right corner
        x_corner_point = 400
        y_corner_point = 0
        x_start = 0.578554471558
    	y_start = -0.185139643848
    	z_start = 0.105740854098
    else:
        limb = 'left'
        cam_controller = 'left_hand_camera'
        hand_range = 'left_hand_range'
        cam_x_offset = -0.025
        cam_y_offset = -0.01
        #top-left corner
        x_corner_point = 400
        y_corner_point = 640
        x_start = 0.584753440252
    	y_start = 0.195382237943
    	z_start = 0.101405569316

    rospy.init_node("object_recognition")

    hover_distance = 0.0 # meters
    moving = Moving(limb, hover_distance)

    # Open the gripper
    moving.gripper_open()

    #Opening camera and getting + saving the image
    camera = CameraController(cam_controller)
    camera.resolution = (width, height)
    camera.open()
    print(cam_controller+' is open.')
    start_orientation = Quaternion(
                             x=0,
                             y=1,
                             z=0,
                             w=0)
    #Moving to start position
    moving._servo_to_pose(Pose(
        position=Point(x=x_start, y=y_start, z=z_start),
        orientation=start_orientation))


    raw_input("Press Enter to take a snapshot")
    #Take and save a snapshot from camera
    msg = rospy.wait_for_message('/cameras/'+cam_controller+'/image', Image)
    camera_image = CvBridge().imgmsg_to_cv2(msg,'bgr8')
    cv2.imwrite(pathSnapshot,camera_image)
    img = cv2.imread(pathSnapshot,cv2.IMREAD_COLOR)
    #Flipping the snapshot
    flip_vertical = cv2.flip(img,1)
    flip_horizontal = cv2.flip(flip_vertical,0)
    img_color = flip_horizontal

    #HOG + SVM
    obj = raw_input('Insert the object to detect: ')
    obj = obj.title()

    box, rotation = HogSVM(img_color,obj, pathResult)
    if box==0 and rotation==0: 
        #Object Detection failed
        sys.exit(0)
    #Find the center of the detected object
    xCenter, yCenter = findCenter(img_color,box,obj)

    
    cv2.rectangle(img_color, (box[5], box[6]), (box[5]+box[3],box[6]+box[4]), (0, 0, 255), thickness=2)
    cv2.circle(img_color, (xCenter, yCenter), 4, (255, 0, 0), -1)
    cv2.imwrite(pathFinalResult, img_color)
    cv2.imshow("Object Detection", img_color)
    cv2.waitKey()
    #cv2.destroyWindow("Object Detection")
    mess=raw_input("Continue? (y/n) ")
    if mess=="n":
        sys.exit(0)




    # Calculation image pixel to baxter point for the pose over the object
    xCenter_pixel_point = yCenter # front back
    yCenter_pixel_point = xCenter # left right
    
    current_pose = moving._limb.endpoint_pose()
    baxter_pose_x = current_pose['position'].x
    print("Old pose, baxter_pose_x:\n{0}".format(baxter_pose_x))
    baxter_pose_y = current_pose['position'].y
    print("Old pose, baxter_pose_y:\n{0}".format(baxter_pose_y))
    
    
    #Calculation center of the object
    xCenter_baxter_point = ((xCenter_pixel_point) - (height/2)) * calibration_factor * distance + baxter_pose_x + cam_x_offset 
    print("New position for xCenter in baxter points:\n{0}".format(xCenter_baxter_point))
    yCenter_baxter_point = ((yCenter_pixel_point) - (width/2)) * calibration_factor * distance + baxter_pose_y + cam_y_offset 
    print("New position for yCenter in baxter points:\n{0}".format(yCenter_baxter_point))

    #Calculation new point in the corner
    x_corner_baxter_point = ((x_corner_point) - (height/2)) * calibration_factor * distance + baxter_pose_x + cam_x_offset 
    print("New position for x_corner in baxter points:\n{0}".format(x_corner_baxter_point))
    y_corner_baxter_point = ((y_corner_point) - (width/2)) * calibration_factor * distance + baxter_pose_y + cam_y_offset 
    print("New position for y_corner in baxter points:\n{0}".format(y_corner_baxter_point))


    # Calculate gripper orientation
    new_orientation = rotate(math.radians(rotation-180),limb)
    #print "rotazione in gradi = {} ---  rotazione in radianti={}".format(rotation,math.radians(rotation))    
    print "New orientation: {}".format(new_orientation)
 
    current_orientation = moving._limb.endpoint_pose()['orientation']
    

    # Moving to the calculated pose
    moving._servo_to_pose(Pose(
        position=Point(x=xCenter_baxter_point, y=yCenter_baxter_point, z=z_start),
        orientation=current_orientation))
    

    current_pose = moving._limb.endpoint_pose()
    baxter_pose_x = current_pose['position'].x
    print("Reached pose, baxter_pose_x:\n{0}".format(baxter_pose_x))
    baxter_pose_y = current_pose['position'].y
    print("Reached pose, baxter_pose_y:\n{0}".format(baxter_pose_y))
    baxter_pose_z = current_pose['position'].z
    print("Reached pose, baxter_pose_z:\n{0}".format(baxter_pose_z))

    #raw_input("w82")

    #Measuring the distance from the object with infrared sensor
    """ 
    # Moving down a little bit before measuring the distance
    # servo to pose
    moving._servo_to_pose(Pose(
        position=Point(x=xCenter_baxter_point, y=yCenter_baxter_point, z=(z_start-0.08)),
        orientation=new_orientation))
    # retract to clear object
    #moving._retract()

    #Measuring the distance
    dist = baxter_interface.analog_io.AnalogIO(hand_range).state()
    print("Distance to the object:\n{0}".format(dist))
  
    dist_value = (dist/1000)-dist_gripper
    print("Distance to the object without gripper distance in centimeters:\n{0}".format(dist_value))

    baxter_pose_z = current_pose['position'].z
    print("Reached pose, baxter_pose_z:\n{0}".format(baxter_pose_z))
    """

    # Rotate the gripper to the correct position
    moving._servo_to_pose(Pose(
        position=Point(x=xCenter_baxter_point, y=yCenter_baxter_point, z=(z_start)),
        orientation=new_orientation))

    # Get down to grip the object
    moving._servo_to_pose(Pose(
        position=Point(x=xCenter_baxter_point, y=yCenter_baxter_point, z=(z_start-0.235)),
        orientation=new_orientation))

    # Close the gripper
    moving.gripper_close()

    # Get up
    moving._servo_to_pose(Pose(
        position=Point(x=xCenter_baxter_point, y=yCenter_baxter_point, z=(z_start)),
        orientation=new_orientation))

    # Moving to the corner 
    moving._servo_to_pose(Pose(
        position=Point(x=x_corner_baxter_point, y=y_corner_baxter_point, z=z_start),
        orientation=new_orientation))

    # Get down to release the object
    moving._servo_to_pose(Pose(
        position=Point(x=x_corner_baxter_point, y=y_corner_baxter_point, z=(z_start-0.235+0.02)),
        orientation=new_orientation))

    # Open the gripper
    moving.gripper_open()

 
    # Get up
    moving._servo_to_pose(Pose(
        position=Point(x=x_corner_baxter_point, y=y_corner_baxter_point, z=(z_start)),
        orientation=new_orientation))


    # Moving to start position
    moving._servo_to_pose(Pose(
        position=Point(x=x_start, y=y_start, z=z_start),
        orientation=start_orientation))

    # Close the gripper
    moving.gripper_close()

if __name__ == '__main__':
    sys.exit(main())
