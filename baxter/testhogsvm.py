#!/usr/bin/python

# Import the required modules
import os
from sklearn.externals import joblib
import cv2
import numpy as np
import datetime

from functions import *


d = 1.10         #downscale for Image Pyramid
ss = [20,20]     #stepsize of the sliding window
mws = [100, 100] #size of the sliding window
th = 0.05        #thresholding value for NMS
ppc = [8, 8]     #pixels_per_cell
cpb = [2, 2]     #cells_per_block
orientations=9   #number of orientation bins
custom_hog=False #flag for using custom hog

visualize_det = False #flag for visualizing the sliding window (it slows down the process)
wait = False    #flag for waiting in every step of the process

ris=[]  #Used in multi-thead implementation for the results of every thread

#Skip some some useless scales to improve the computational time
def checkScale(scale,obj):
    if obj=="Banana":
        if scale<7:
            return False
    elif obj=="Box": 
        if scale<8:
            return False
    elif obj=="Orange": 
        if scale<1:
            return False
    elif obj=="Pocket": 
        if scale<2:
            return False
    elif obj=="Book":
        if scale<10:
            return False
    elif obj=="Bottle":
        #TOP
        if scale<6:
            return False
        #FRONT
        #if scale<11:
        #    return False
    elif obj=="Glasses":
        #CLOSE
        if scale<4:
            return False
        #OPEN
        #if scale<6:
        #    return False
    elif obj=="Key":
        if scale<8:
            return False

    return True

#Find the contour with max area
def findMAxContour(contours):
    maxc = 0
    cnt = []
    for i in contours:
	    if len(i) > 5:
            	# Calculate contour area
            	A = cv2.contourArea(i)
            	if A > maxc:
                	maxc = A
                    	cnt = i
    return cnt 

#Find the center of the detected object
def findCenter(img,maxt3,obj):
    img = img[maxt3[6]:maxt3[6]+maxt3[4], maxt3[5]:maxt3[5]+maxt3[3]]

    hsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
    #cv2.imshow('HSV',hsv)
    #cv2.waitKey()
    if obj=="Banana":
        lower = np.array([10,10,50])
        upper = np.array([30,255,255])
    elif obj=="Box": 
        lower = np.array([110,50,50])
        upper = np.array([130,255,255])
    elif obj=="Orange": 
        lower = np.array([0,10,50])
        upper = np.array([20,255,255])
    elif obj=="Pocket": 
        lower = np.array([0,10,10])
        upper = np.array([30,255,255])
    elif obj=="Book":
        lower = np.array([0,0,220])
        upper = np.array([255,30,255])
    elif obj=="Bottle":
        #TOP
        lower = np.array([110,50,50])
        upper = np.array([130,255,255])
        #FRONT
        #lower = np.array([110,50,50])
        #upper = np.array([130,255,255])
    elif obj=="Glasses":
        #CLOSE
        lower = np.array([110,50,50])
        upper = np.array([130,255,255])
        #OPEN
        #lower = np.array([110,50,50])
        #upper = np.array([130,255,255])
    elif obj=="Key":
        lower = np.array([0,10,50])
        upper = np.array([20,255,255])

    # Threshold the HSV image
    mask = cv2.inRange(hsv, lower, upper)
    #dilate and erode the mask for better result
    kernel = np.ones((3,3), np.uint8)
    mask = cv2.dilate(mask, kernel, iterations=5)
    mask = cv2.erode(mask,kernel,iterations = 5)

    # Bitwise-AND mask and original image
    #res = cv2.bitwise_and(img,img, mask= mask)
    #Display results
    #cv2.imshow('frame',img)
    #cv2.waitKey()
    #cv2.imshow('mask',mask)
    #cv2.waitKey()
    #cv2.imshow('res',res)
    #cv2.waitKey()

    
    #Find and draw contours of the mask
    contours, _ = cv2.findContours(mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    cnt = findMAxContour(contours)
    cv2.drawContours(img,[cnt],0,(0,0,255),2)
    #cv2.imshow('frame',img)
    #cv2.waitKey()

    # Calcuate moments of the mask to find the center
    M = cv2.moments(cnt)
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
 
    # draw the center of the mask
    cv2.circle(img, (cX, cY), 4, (255, 0, 0), -1)
    #cv2.imshow('frame',img)
    #cv2.waitKey()

    #Translate the relative center on the snapshot
    cX=cX+maxt3[5]
    cY=cY+maxt3[6]
    return (cX,cY)

#Rotate the sliding window every 10 degrees and compute HOG features
def rotate_and_predict(img,clf):
    (h, w) = img.shape[:2]
    center = (w / 2, h / 2)
    ris=[]
    el=[]
    flag=False
    for angle in range(0,360,10):
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotated = cv2.warpAffine(img, M, (w, h))

        if custom_hog==False:
            object=""
        else:
            object=obj
        fd = hog_cv(rotated,ppc,cpb,orientations,object)
        fd = fd.reshape(1, -1)
        pred = clf.predict(fd)
        if pred == 1:
            el.append([pred,fd,angle])
            flag=True
        else:
            if flag==True:
                flag=False
                ris.append(el)
                el=[]
    if flag==True:
        #Join the last element with the first one
        if len(ris)==0:
            ris.append(el)
        else:
            for j in el:
                ris[0].append([j[0],j[1],j[2]-360])

    if len(ris) == 0:
        ris.append(0)
        return ris
    else:
        #Find the array with the max length
        max=[]
        for i in ris:
            if len(i)>len(max):
                max=i
        #Find the element with the max decision_function()
        maxp=[]
        for i in max:
            if len(maxp)==0:
                maxp=i
            else:
                if clf.decision_function(i[1])> clf.decision_function(maxp[1]):
                    maxp=i

        return maxp

#Resize and crop the snapshot to find the detection saved in maxt (we use this implementation because it doesn't have smoothing)
def pyramidCrop(snap,img,maxt):
    scale=0
    (h, w) = img.shape[:2]
    for snap_scaled in pyramid(snap, 1.10, [100,100]):
        scalato=int(100*(1.10**scale))
        if scalato>=h:
            break
        scale+=1
    resized=snap_scaled
    crop=resized[maxt[1]:(maxt[1]+100),maxt[0]:(maxt[0]+100)] # [y:(y+h), x:(x+w)]
    return crop

#Find a more accurate orientation (error +- 2 degrees)
def findOrientation(img,clf,obj):
    (h, w) = img.shape[:2]
    center = (w / 2, h / 2)
    ris=[]
    el=[]
    flag=False
    for angle in range(0,360,2):
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotated = cv2.warpAffine(img, M, (w, h))

        if custom_hog==False:
            object=""
        else:
            object=obj
        fd = hog_cv(rotated,ppc,cpb,orientations,object)
        fd = fd.reshape(1, -1)
        pred = clf.predict(fd)
        if pred == 1:
            el.append(angle)
            flag=True
        else:
            if flag==True:
                flag=False
                ris.append(el)
                el=[]
    if flag==True:
        #Join the last element with the first one
        if len(ris)==0:
            ris.append(el)
        else:
            for j in el:
                ris[0].append(j-360)

    if len(ris) == 0:
        ris.append(0)
        return ris
    else:
        #print ris
        #Find the array with the max length
        max=[]
        for i in ris:
            if len(i)>len(max):
                max=i
        #Find the median element of that vector
        media=np.median(max, axis=0)
	media=int(media)

        #Fix the object's orientation based on positive images' orientation
        if obj=="Banana": 
            media=media
        elif obj=="Box": 
            media=media-2
        elif obj=="Orange": 
            media=media
        elif obj=="Pocket": 
            media=media-90
        elif obj=="Book":
            media=media
        elif obj=="Bottle":
            #TOP
            media=media
            #FRONT
            #media=media
        elif obj=="Glasses":
            #CLOSE
            media=media-88
            #OPEN
            #media=media
        elif obj=="Key":
            media=media

        return media


""" #MULTI-THREAD IMPLEMENTATION
def thread_funz(barriera,evento,imgs,clf,cont,scale):
    #print "Thread {}: inizio    len(imgs)={}".format(cont,len(imgs))
    global ris
    detections=[]
    gradi=[]
    for (x, y, im_window) in imgs:
        if im_window.shape[0] != mws[1] or im_window.shape[1] != mws[0]:
            continue
        # Calculate the HOG features
        out=rotate_and_predict(im_window,clf)
        pred=out[0]
        if pred == 1:
            fd=out[1]
            #print  "Detection:: Location -> ({}, {}) ({}, {})".format(x, y, int(mws[0]*(d**scale)), int(mws[1]*(d**scale)))
            #print "Scale ->  {} | Confidence Score {} \n".format(scale, clf.decision_function(fd))
            detections.append((x, y, clf.decision_function(fd), int(mws[0]*(d**scale)), int(mws[1]*(d**scale)), int(x*(d**scale)), int(y*(d**scale))))
            gradi.append(out[2])
    ris[cont]=(detections,gradi)
    #print "     Thread {}: trovati {} Detections".format(cont,len(detections))
    barriera.enter()
    evento.post()


def sliding_window_thread(img, mws, ss,n_thread,clf,scale):
    global ris
    ris=[]
    for i in range(0,n_thread):
        ris.append(())
    #print "ris={}".format(ris)
    #print "len(ris)={}".format(len(ris))

    windows=sliding_window2(img, mws, ss)
    #divido le finestre tra i vari thread
    diviso=len(windows)/n_thread
    resto=len(windows)%n_thread
    #print "len(windows)={}      diviso={}   resto={}".format(len(windows),diviso,resto)

    win=[windows[i:i+diviso] for i in xrange(0, len(windows), diviso)]
    #for k in range(0,len(win)):
    #    print "win[{}]={}".format(k,len(win[k]))

    if resto>0:
        for i in range(0,resto):
            win[i].append(win[n_thread][i])
        win.pop()

    #for k in range(0,len(win)):
    #    print "win[{}]={}".format(k,len(win[k]))
    
    bar=ba.barrier(n_thread)
    finished=ba.event()
    for i in range(0,n_thread):
        #thread.start_new_thread(thread_funz,(bar,finished,win[i],clf,i,scale))
        thread = t.Thread(target=thread_funz, args=(bar,finished,win[i],clf,i,scale))
        thread.start()
    
    finished.wait()
    #print "Tutti i thread hanno finito"
    final_ris=([],[])
    for det,gradi in ris:
        for j in det:
            final_ris[0].append(j)
        for k in gradi:
            final_ris[1].append(k)
    return final_ris    
"""

#HOG + SVM implementation:
#   im=snapshot
#   obj=object to detect in the snapshot
#   contt= parameter used when there are multiple snapshots
def HogSVM(im,obj,result_path, snap_number=1):

    print "time={}".format(datetime.datetime.now())
    
    #path of the classifier model
    model_path = "models/{}.model".format(obj)
    
    result=result_path+"/{}Detections-".format(snap_number)+obj
    resultFounded=result_path+"/{}Founded-".format(snap_number)+obj
    resultFoundedRotated=result_path+"/{}FoundedRotated-".format(snap_number)+obj 
   

    # Load the classifier
    clf = joblib.load(model_path)

    # List to store the detections and their angles
    detections = []
    angles =[]
    # The current scale of the image
    scale = 0
    # Downscale the image and iterate
    for im_scaled in pyramid(im, d, mws):
        print "Scale: {}".format(scale)
        # This list contains detections at the current scale
        cd = []
	if checkScale(scale,obj)==False:
            scale+=1
            #cv2.imwrite(result_path+"screen{}-{}.png".format(snap_number,scale),im_scaled)
            continue
        # If the width or height of the scaled image is less than
        # the width or height of the window, then end the iterations.
        if im_scaled.shape[0] < mws[1] or im_scaled.shape[1] < mws[0]:
            break
        #Sliding window - SINGLE-THREAD IMPLEMENTATION
        for (x, y, im_window) in sliding_window(im_scaled, mws, ss):
            if im_window.shape[0] != mws[1] or im_window.shape[1] != mws[0]:
                continue
            # Calculate the HOG features
            ris=rotate_and_predict(im_window,clf)
            pred=ris[0]
            if pred == 1:
                fd=ris[1]
                #print  "Detection:: Location -> ({}, {}) ({}, {})".format(x, y, int(mws[0]*(d**scale)), int(mws[1]*(d**scale)))
                #print "Scale ->  {} | Confidence Score {} \n".format(scale, clf.decision_function(fd))
                detections.append((x, y, clf.decision_function(fd), int(mws[0]*(d**scale)), int(mws[1]*(d**scale)), int(x*(d**scale)), int(y*(d**scale))))
                cd.append(detections[-1])
                angles.append(ris[2])
            # If visualize is set to true, display the working
            # of the sliding window
            if visualize_det:
                clone = im_scaled.copy()
                for x1, y1, _, _, _, _, _  in cd:
                    # Draw the detections at this scale
                    cv2.rectangle(clone, (x1, y1), (x1 + im_window.shape[1], y1 + im_window.shape[0]), (0, 0, 0), thickness=2)
                cv2.rectangle(clone, (x, y), (x + im_window.shape[1], y + im_window.shape[0]), (255, 255, 255), thickness=2)
                cv2.imshow("Sliding Window in Progress", clone)
                if wait:
                    cv2.waitKey(30)
        """ #Sliding window - MULTI-THREAD IMPLEMENTATION
        ris=sliding_window_thread(im_scaled, mws, ss,4,clf,scale)
        cd=ris[0]
        for i in range(0,len(ris[0])):
            detections.append(ris[0][i])
            angles.append(ris[1][i])
        """
        # Move the the next scale
        print "   {} Detections Founded".format(len(cd))
        scale += 1



    # Find the best detection and save it in maxt1, while maxind is the index of its angle
    clone = im.copy()
    maxd=0
    maxt1=[]
    maxind=0
    ind=0
    for (x_tl, y_tl, decision, w, h, x_mod, y_mod) in detections:
        if decision[0] > maxd:
            maxd=decision[0]
            maxt1=(x_tl, y_tl, decision, w, h, x_mod, y_mod)
            maxind=ind
        ind+=1
    

    
    # Perform Non Maxima Suppression
    th_detections = nms(detections, th)

    # Find the best th_detection and save it in maxt3
    th_clone = clone.copy()
    maxd=0
    maxt3=[]
    if len(th_detections)>0:
        # Display the results after performing NMS
        for (x_tl, y_tl, decision, w, h, x_mod, y_mod) in th_detections:
            # Draw the detections
            cv2.rectangle(th_clone, (x_mod, y_mod), (x_mod+w,y_mod+h), (0, 0, 0), thickness=2)
            if decision[0] > maxd:
                maxd=decision[0]
                maxt3=(x_tl, y_tl, decision, w, h, x_mod, y_mod)
        if visualize_det:
            cv2.imshow("Final Detections after applying NMS with threshold={}".format(th), th_clone)
        cv2.imwrite(result+'-{}-nms.png'.format(th),th_clone)
        if wait:
            cv2.waitKey()
    
    #Display ALL the results:
    imgClone=clone.copy()
    if len(angles)==0:
        angles.append(0)
    print "     Thread {} - Detections founded: {}".format(obj,len(detections))
    print "time={}".format(datetime.datetime.now())
    #Display a blue rectangle for the best detection:
    if len(detections)>0:
        cv2.rectangle(clone, (maxt1[5]+2, maxt1[6]+2), (maxt1[5]+maxt1[3]+2,maxt1[6]+maxt1[4]+2), (255, 0, 0), thickness=2)
        if len(detections)<10:
            for (x_tl, y_tl, decision, w, h, x_mod, y_mod) in detections:
                cv2.rectangle(clone, (x_mod, y_mod), (x_mod+w, y_mod+h), (0, 0, 0), thickness=2)
        if visualize_det:
            cv2.imshow("Final Detections1 BLU".format(th), clone)
            cv2.waitKey()
    #Display a red rectangle for the best th_detection:
    if len(th_detections)>0:
        cv2.rectangle(clone, (maxt3[5], maxt3[6]), (maxt3[5]+maxt3[3],maxt3[6]+maxt3[4]), (0, 0, 255), thickness=2)
        if visualize_det:
            cv2.imshow("Final Detections3 RED".format(th), clone)
            cv2.waitKey()

    font = cv2.FONT_HERSHEY_PLAIN
    cv2.putText(clone,'Detections: {} - {} - Angles:{}'.format(len(detections),len(th_detections),angles[maxind]),(20,20), font, 1,(0,128,255),2)
    cv2.imwrite(result+'-{}.png'.format(th),clone)

    if len(maxt3)>0:
        #object founded
        imgFounded=imgClone[maxt3[6]:(maxt3[6]+maxt3[4]), maxt3[5]:(maxt3[5]+maxt3[3])]
        imgFounded2=imgFounded
        cv2.imwrite(resultFounded+'-{}.png'.format(th),imgFounded)

        #simulation of the rotated object
        (h, w) = imgFounded.shape[:2]
        center = (w / 2, h / 2)
        M = cv2.getRotationMatrix2D(center, angles[maxind], 1.0)
        rotated = cv2.warpAffine(imgFounded, M, (w, h))
        cv2.imwrite(resultFoundedRotated+'-{}.png'.format(th),rotated)

        #Find a more accurate orientation
        imgFounded=pyramidCrop(im,imgFounded,maxt3)
        bestAngle=findOrientation(imgFounded,clf,obj)
        
        print "bestAngle={}".format(bestAngle)

        #simulation of the rotated object with bestAngle
        (h, w) = imgFounded2.shape[:2]
        center = (w / 2, h / 2)
        M = cv2.getRotationMatrix2D(center, bestAngle, 1.0)
        rotated = cv2.warpAffine(imgFounded2, M, (w, h))
        cv2.imwrite(resultFoundedRotated+'2-{}.png'.format(th),rotated)

        return (maxt3,bestAngle)
    else:
        #Object Detection failed
        print "Object Detection failed!"
        return (0,0)



