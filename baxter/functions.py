import cv2
import imutils 
import numpy as np
import os
import glob
import shutil
import pybarrier as ba
import thread
import threading as t

#Blocks' indexes to implement a custom hog for the orange 
def circle():
    indexes=[]
    for i in range(0,11):
        if i==0:
            min=4
            max=7
        if i==1:
            min=3
            max=8
        if i==2:
            min=2
            max=9
        if i==3:
            min=1
            max=10
        if i==4:
            min=0
            max=11
        if i==5:
            min=0
            max=11
        if i==6:
            min=0
            max=11
        if i==7:
            min=1
            max=10
        if i==8:
            min=2
            max=9
        if i==9:
            min=3
            max=8
        if i==10:
            min=4
            max=7  
        for j in range(min,max):
            ind=i*11+j
            indexes.append(ind)
    
    return indexes

#Blocks' indexes to implement a custom hog for the banana
def rectBanana():
    indexes=[]
    for i in range(0,11):
        if i==0:
            min=0
            max=0
        if i==1:
            min=0
            max=0
        if i==2:
            min=10
            max=11
        if i==3:
            min=9
            max=11
        if i==4:
            min=8
            max=11
        if i==5:
            min=0
            max=11
        if i==6:
            min=0
            max=11
        if i==7:
            min=0
            max=11
        if i==8:
            min=4
            max=7
        if i==9:
            min=0
            max=0
        if i==10:
            min=0
            max=0  
        for j in range(min,max):
            ind=i*11+j
            indexes.append(ind)
    
    return indexes

#Blocks' indexes to implement a custom hog for the box
def rectBox():
    indexes=[]
    for i in range(0,11):
        if i==0:
            min=0
            max=0
        if i==1:
            min=0
            max=11
        if i==2:
            min=0
            max=11
        if i==3:
            min=0
            max=11
        if i==4:
            min=0
            max=11
        if i==5:
            min=0
            max=11
        if i==6:
            min=0
            max=11
        if i==7:
            min=0
            max=11
        if i==8:
            min=0
            max=11
        if i==9:
            min=0
            max=11
        if i==10:
            min=0
            max=0  
        for j in range(min,max):
            ind=i*11+j
            indexes.append(ind)
    
    return indexes
#Blocks' indexes to implement a custom hog for the pocket
def rectPocket():
    indexes=[]
    for i in range(0,11):
        if i==0:
            min=1
            max=9
        if i==1:
            min=1
            max=9
        if i==2:
            min=1
            max=9
        if i==3:
            min=1
            max=9
        if i==4:
            min=1
            max=9
        if i==5:
            min=1
            max=9
        if i==6:
            min=1
            max=9
        if i==7:
            min=1
            max=9
        if i==8:
            min=1
            max=9
        if i==9:
            min=1
            max=9
        if i==10:
            min=1
            max=9  
        for j in range(min,max):
            ind=i*11+j
            indexes.append(ind)
    
    return indexes
#Blocks' indexes to implement a custom hog for the book
def rectBook():
    indexes=[]
    for i in range(0,11):
        if i==0:
            min=0
            max=0
        if i==1:
            min=0
            max=11
        if i==2:
            min=0
            max=11
        if i==3:
            min=0
            max=11
        if i==4:
            min=0
            max=11
        if i==5:
            min=0
            max=11
        if i==6:
            min=0
            max=11
        if i==7:
            min=0
            max=11
        if i==8:
            min=0
            max=11
        if i==9:
            min=0
            max=11
        if i==10:
            min=0
            max=0  
        for j in range(min,max):
            ind=i*11+j
            indexes.append(ind)
    
    return indexes
#Blocks' indexes to implement a custom hog for the bottle (front view)
def rectBottleFront():
    indexes=[]
    for i in range(0,11):
        if i==0:
            min=0
            max=0
        if i==1:
            min=0
            max=0
        if i==2:
            min=0
            max=0
        if i==3:
            min=0
            max=0
        if i==4:
            min=0
            max=11
        if i==5:
            min=0
            max=11
        if i==6:
            min=0
            max=11
        if i==7:
            min=3
            max=11
        if i==8:
            min=0
            max=0
        if i==9:
            min=0
            max=0
        if i==10:
            min=0
            max=0  
        for j in range(min,max):
            ind=i*11+j
            indexes.append(ind)
    
    return indexes
#Blocks' indexes to implement a custom hog for the bottle (top view)
def rectBottleTop():
    indexes=[]
    for i in range(0,11):
        if i==0:
            min=0
            max=0
        if i==1:
            min=2
            max=11
        if i==2:
            min=1
            max=11
        if i==3:
            min=0
            max=11
        if i==4:
            min=0
            max=11
        if i==5:
            min=0
            max=11
        if i==6:
            min=0
            max=11
        if i==7:
            min=0
            max=11
        if i==8:
            min=0
            max=11
        if i==9:
            min=1
            max=10
        if i==10:
            min=0
            max=0  
        for j in range(min,max):
            ind=i*11+j
            indexes.append(ind)
    
    return indexes
#Blocks' indexes to implement a custom hog for the key
def rectKey():
    indexes=[]
    for i in range(0,11):
        if i==0:
            min=0
            max=0
        if i==1:
            min=0
            max=0
        if i==2:
            min=0
            max=0
        if i==3:
            min=8
            max=11
        if i==4:
            min=0
            max=11
        if i==5:
            min=0
            max=11
        if i==6:
            min=0
            max=11
        if i==7:
            min=0
            max=11
        if i==8:
            min=8
            max=11
        if i==9:
            min=0
            max=0
        if i==10:
            min=0
            max=0  
        for j in range(min,max):
            ind=i*11+j
            indexes.append(ind)
    
    return indexes
#Blocks' indexes to implement a custom hog for the closed glasses
def rectGlassesClosed():
    indexes=[]
    for i in range(0,11):
        if i==0:
            min=4
            max=8
        if i==1:
            min=4
            max=8
        if i==2:
            min=4
            max=8
        if i==3:
            min=4
            max=8
        if i==4:
            min=4
            max=8
        if i==5:
            min=4
            max=8
        if i==6:
            min=4
            max=8
        if i==7:
            min=4
            max=8
        if i==8:
            min=4
            max=8
        if i==9:
            min=4
            max=8
        if i==10:
            min=4
            max=8  
        for j in range(min,max):
            ind=i*11+j
            indexes.append(ind)
    
    return indexes
#Blocks' indexes to implement a custom hog for the open glasses
def rectGlassesOpen():
    indexes=[]
    for i in range(0,11):
        if i==0:
            min=1
            max=11
        if i==1:
            min=1
            max=3
        if i==2:
            min=1
            max=3
        if i==3:
            min=1
            max=3
        if i==4:
            min=1
            max=3
        if i==5:
            min=1
            max=3
        if i==6:
            min=0
            max=3
        if i==7:
            min=0
            max=3
        if i==8:
            min=0
            max=3
        if i==9:
            min=0
            max=3
        if i==10:
            min=1
            max=11  
        for j in range(min,max):
            ind=i*11+j
            indexes.append(ind)
    
    return indexes
#Filtering the features of an object by using a custom hog
#   hog= features vector of the image
#   object= object detected
def customHog(hog,object):
    indexes=[]
    if object=="Banana":    #Group1
        indexes=rectBanana()
    elif object=="Box":
        indexes=rectBox()
    elif object=="Orange":
        indexes=circle()
    elif object=="Pocket":
        indexes=rectPocket()
    elif object=="Book":    #Group2
        indexes=rectBook()
    elif object=="Bottle":
        #indexes=rectBottleFront()
        indexes=rectBottleTop()
    elif object=="Glasses":
        #indexes=rectGlassesOpen()
        indexes=rectGlassesClosed()
    elif object=="Key":
        indexes=rectKey()
    new_hog=[]
    for i in indexes:
        for j in range(0,36):
            k=i*36+j
            new_hog.append(hog[k])
    
    return new_hog

"""
#Make new features by calculating average of the RGB channels
#   fd= features vector of the image
def colorFeatures(fd):
    mediaB=np.mean(img[:,:,0], axis=1)
    mediaG=np.mean(img[:,:,1], axis=1)
    mediaR=np.mean(img[:,:,2], axis=1)
    mediaAllB=np.mean(mediaB)
    mediaAllG=np.mean(mediaG)
    mediaAllR=np.mean(mediaR)
    fd.append(mediaAllB)
    fd.append(mediaAllG)
    fd.append(mediaAllR)
    return fd
"""

"""
HOG implementation by using OpenCV:
    img_path = path of the image or the image already loaded
    cell_size = (h , w) in pixels
    block_size = (h , w) in cells
    nbins = number of orientation bins
    object = object to detect ( if =="" then doesn't use custom hog)
"""
def hog_cv(img_path,cell_size,block_size,nbins,object):
    if isinstance(img_path, basestring):
        img = cv2.imread(img_path,cv2.IMREAD_COLOR)
    else:
        img=img_path

    # winSize is the size of the image cropped to an multiple of the cell size
    hogg = cv2.HOGDescriptor(_winSize=(img.shape[1] // cell_size[1] * cell_size[1],
                                 img.shape[0] // cell_size[0] * cell_size[0]),
                        _blockSize=(block_size[1] * cell_size[1],
                                    block_size[0] * cell_size[0]),
                        _blockStride=(cell_size[1], cell_size[0]),
                        _cellSize=(cell_size[1], cell_size[0]),
                        _nbins=nbins)
    hog_feats = hogg.compute(img)
    
    if object!="":
        #custom hog
        hog_feats=customHog(hog_feats,object)
    fdnew=[]
    for el in hog_feats:
        fdnew.append(el[0])
    #Add new features by calculating average of the RGB channels
    #fdnew=colorFeatures(img,fdnew)
    return np.array(fdnew)
"""
#Another implementation of the feature extraction, by splitting the RGB channels, computing HOG features for every channel, and concatenating all the features
def hog_cvv(img_path,cell_size,block_size,nbins,object):
    if isinstance(img_path, basestring):
        img = cv2.imread(img_path,cv2.IMREAD_COLOR)
    else:
        img=img_path
    b,g,r = cv2.split(img)
    hogB=hog_cv2(b,cell_size,block_size,nbins,object)
    hogG=hog_cv2(g,cell_size,block_size,nbins,object)
    hogR=hog_cv2(r,cell_size,block_size,nbins,object)
    hogBG=np.concatenate((hogB,hogG),axis=0)
    hogBGR=np.concatenate((hogBG,hogR),axis=0)
    return hogBGR
"""

#Image Pyramid implementation without smoothing (better for HOG)
def pyramid(image, scale, minSize):
    # yield the original image
    yield image
    # keep looping over the pyramid
    while True:
		# compute the new dimensions of the image and resize it
        wi = int(image.shape[1] / scale)
        image = imutils.resize(image, width=wi)

		# if the resized image does not meet the supplied minimum
		# size, then stop constructing the pyramid
        if image.shape[0] < minSize[1] or image.shape[1] < minSize[0]:
            break

		# yield the next image in the pyramid
        yield image

#Implementation of the sliding window
def sliding_window(image, window_size, step_size):
    '''
    This function returns a patch of the input image `image` of size equal
    to `window_size`. The first image returned top-left co-ordinates (0, 0) 
    and are increment in both x and y directions by the `step_size` supplied.
    So, the input parameters are -
    * `image` - Input Image
    * `window_size` - Size of Sliding Window
    * `step_size` - Incremented Size of Window

    The function returns a tuple -
    (x, y, im_window)
    where
    * x is the top-left x co-ordinate
    * y is the top-left y co-ordinate
    * im_window is the sliding window image
    '''
    for y in xrange(0, image.shape[0], step_size[1]):
        for x in xrange(0, image.shape[1], step_size[0]):
            yield (x, y, image[y:y + window_size[1], x:x + window_size[0]])


"""
#Second implementation of the sliding window used for multi-threading
def sliding_window2(image, window_size, step_size):
    out=[]
    for y in xrange(0, image.shape[0], step_size[1]):
        for x in xrange(0, image.shape[1], step_size[0]):
            out.append( (x, y, image[y:y + window_size[1], x:x + window_size[0]]))
    return out
"""

def overlapping_area(detection_1, detection_2):
    '''
    Function to calculate overlapping area'si
    `detection_1` and `detection_2` are 2 detections whose area
    of overlap needs to be found out.
    Each detection is list in the format ->
    [x-top-left, y-top-left, confidence-of-detections, width-of-detection, height-of-detection]
    The function returns a value between 0 and 1,
    which represents the area of overlap.
    0 is no overlap and 1 is complete overlap.
    Area calculated from ->
    http://math.stackexchange.com/questions/99565/simplest-way-to-calculate-the-intersect-area-of-two-rectangles
    '''
    # Calculate the x-y co-ordinates of the 
    # rectangles
    x1_tl = detection_1[0]
    x2_tl = detection_2[0]
    x1_br = detection_1[0] + detection_1[3]
    x2_br = detection_2[0] + detection_2[3]
    y1_tl = detection_1[1]
    y2_tl = detection_2[1]
    y1_br = detection_1[1] + detection_1[4]
    y2_br = detection_2[1] + detection_2[4]
    # Calculate the overlapping Area
    x_overlap = max(0, min(x1_br, x2_br)-max(x1_tl, x2_tl))
    y_overlap = max(0, min(y1_br, y2_br)-max(y1_tl, y2_tl))
    overlap_area = x_overlap * y_overlap
    area_1 = detection_1[3] * detection_2[4]
    area_2 = detection_2[3] * detection_2[4]
    total_area = area_1 + area_2 - overlap_area
    return overlap_area / float(total_area)

def nms(detections, threshold=.5):
    '''
    This function performs Non-Maxima Suppression.
    `detections` consists of a list of detections.
    Each detection is in the format ->
    [x-top-left, y-top-left, confidence-of-detections, width-of-detection, height-of-detection]
    If the area of overlap is greater than the `threshold`,
    the area with the lower confidence score is removed.
    The output is a list of detections.
    '''
    if len(detections) == 0:
	return []
    # Sort the detections based on confidence score
    detections = sorted(detections, key=lambda detections: detections[2],
            reverse=True)
    # Unique detections will be appended to this list
    new_detections=[]
    # Append the first detection
    new_detections.append(detections[0])
    # Remove the detection from the original list
    del detections[0]
    # For each detection, calculate the overlapping area
    # and if area of overlap is less than the threshold set
    # for the detections in `new_detections`, append the 
    # detection to `new_detections`.
    # In either case, remove the detection from `detections` list.
    for index, detection in enumerate(detections):
        for new_detection in new_detections:
            if overlapping_area(detection, new_detection) > threshold:
                del detections[index]
                break
        else:
            new_detections.append(detection)
            del detections[index]
    return new_detections
