'''
Set the config variable.
'''

import ConfigParser as cp
import json

config = cp.RawConfigParser()
config.read('./config/config.cfg')

min_wdw_sz = json.loads(config.get("hog","min_wdw_sz"))
step_size = json.loads(config.get("hog", "step_size"))
orientations = config.getint("hog", "orientations")
pixels_per_cell = json.loads(config.get("hog", "pixels_per_cell"))
cells_per_block = json.loads(config.get("hog", "cells_per_block"))
block_norm = config.get("hog", "block_norm")
transform_sqrt = config.getboolean("hog", "transform_sqrt")
visualise = config.getboolean("hog", "visualise")
downscale = config.getfloat("hog", "downscale")
neg_type = config.get("hog", "neg_type")
custom_hog = config.getboolean("hog", "custom_hog")

threshold = config.getfloat("nms", "threshold")

pos_feat_path = config.get("paths", "pos_feat_path")
neg_feat_path = config.get("paths", "neg_feat_path")
model_path = config.get("paths", "model_path")
test_im_path = config.get("paths", "test_im_path")
result_path = config.get("paths", "result_path")


test_im_path1 = config.get("test", "test_im_path1")
test_im_path2 = config.get("test", "test_im_path2")
group=config.getint("test","group")
obj1 = json.loads(config.get("test", "obj1"))
obj2 = json.loads(config.get("test", "obj2"))