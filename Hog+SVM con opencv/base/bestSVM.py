import os
#import sys
import glob
#import shutil
import datetime
#from config import *
import numpy as np
#import threading as t  

#from skimage.feature import local_binary_pattern
from sklearn.svm import LinearSVC
from sklearn import svm
#from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import GridSearchCV

pos_feat_path="../data/features/pos"
neg_feat_path="../data/features/neg"
model_path="../data/models/nuovo/svm.model"
def train():
    # Perform training
    #print "Inizio train"

    #Carico le features
    fds = []
    labels = []
    # Load the positive features
    for feat_path in glob.glob(os.path.join(pos_feat_path,"*.feat")):        
        fd = joblib.load(feat_path)
        length_array_positives = len(fd)
        fds.append(fd)
        labels.append(1)
    #print "Positive features loaded."
    # Load the negative features
    for feat_path in glob.glob(os.path.join(neg_feat_path,"*.feat")):
        fd = joblib.load(feat_path)
        length_array_negatives = len(fd)       
        fds.append(fd)
        labels.append(0)
    #print "Negative features loaded."

    C_range = np.logspace(-5, 4, 10)
    out=bestLinear(fds, labels,C_range,-5,4,0)
    print "Fine Linear {}".format(datetime.datetime.now())
    clf = LinearSVC(C=out)
    print "Training a Linear SVM Classifier with C={}".format(out)
    clf.fit(fds, labels)
    # If feature directories don't exist, create them
    if not os.path.isdir(os.path.split(model_path)[0]):
        os.makedirs(os.path.split(model_path)[0])
    joblib.dump(clf, model_path)
    #print "Classifier saved to {}".format(model_path)



def bestLinear(fds, labels, C_range, ini, fin, cont):
    print "INIZIO Linear {} {} {}: {}".format(cont, ini, fin, datetime.datetime.now())
    
    print C_range
    param_grid = dict(C=C_range)
    cv = StratifiedShuffleSplit(n_splits=5, test_size=0.2, random_state=42)
    grid = GridSearchCV(svm.LinearSVC(), param_grid=param_grid, cv=cv)
    grid.fit(fds, labels)

    print("The best parameters are %s with a score of %0.2f" % (grid.best_params_, grid.best_score_))
    out=1
    for ind,val in np.ndenumerate(C_range):
        if val==grid.best_params_['C']:
            if ind[0]==0:
                print "ZERO"
                if cont==0:
                    ini=ini-8
                    fin=fin-8
                    C_range=np.logspace(ini, fin, 10)
                else:
                    ini=val-((fin-val)*10)
                    fin=C_range[ind[0]+1]
                    C_range=np.linspace(ini, fin, 10)
                out=bestLinear(fds, labels, C_range, ini, fin, cont)
            elif ind[0]==9:
                print "NOVE"
                if cont==0:
                    ini=ini+8
                    fin=fin+8
                    C_range=np.logspace(ini, fin, 10)
                else:
                    ini=C_range[ind[0]-1]
                    fin=val+((val-ini)*10)
                    C_range=np.linspace(ini, fin, 10)
                out=bestLinear(fds, labels, C_range, ini, fin, cont)
            else:
                print "ALTRO"
                if cont==10:
                    out=val
                    break
                
                if cont==0:
                    ini=ini+ind[0]-1
                    fin=ini+2
                    C_range=np.linspace(10**ini, 10**fin, 10)
                else:
                    ini=C_range[ind[0]-1]
                    fin=C_range[ind[0]+1]
                    C_range=np.linspace(ini, fin, 10)
                cont+=1
                out=bestLinear(fds, labels, C_range, ini, fin, cont)

    return out

train()