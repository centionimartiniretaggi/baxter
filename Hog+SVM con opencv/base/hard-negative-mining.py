import os
import argparse as ap
from skimage.transform import pyramid_gaussian
from skimage.io import imread
from skimage.feature import hog
from sklearn.externals import joblib
import cv2
import imutils 
import numpy as np
from nms import nms
from config import *
import glob
import shutil
from functions import *



if __name__ == "__main__":
    parser = ap.ArgumentParser()
    parser.add_argument('-d', '--downscale', help="Downscale ratio", default=downscale, type=float)
    parser.add_argument('-ss', '--step_size', help="step_size", default=step_size, nargs='+', type=int)
    parser.add_argument('-mws', '--min_wdw_sz', help="min_wdw_sz", default=min_wdw_sz, nargs='+', type=int)
    parser.add_argument('-v', '--visualize', help="Visualize the sliding window", action="store_true")
    parser.add_argument('-w', '--wait', help="Wait for the result", action="store_true")
    parser.add_argument('-th', '--threshold', help="threshold", default=[threshold], nargs='+', type=float)
    parser.add_argument('-ppc', "--pixels_per_cell", help="pixels_per_cell", nargs='+', type=int, default=pixels_per_cell)
    parser.add_argument('-cpb', "--cells_per_block", help="cells_per_block", nargs='+', type=int, default=cells_per_block)
    parser.add_argument('-bk', "--block_norm", help="block_norm", default=block_norm)
    parser.add_argument('-ts', '--transform_sqrt', help="transform_sqrt", action="store_true")
    parser.add_argument('-obj', "--object", help="Selected Object", required=True)
    parser.add_argument('-cont', "--contatore", help="contatore", default="0", type=str)
    
    args = vars(parser.parse_args())

    d = args['downscale']
    visualize_det = args['visualize']
    wait = args['wait']
    bk = args["block_norm"]
    ts = args["transform_sqrt"]
    obj= args["object"]
    cont = args["contatore"]
    if cont!="0":
        neg_feat_path = "../data/features/neg{}".format(cont)
        model_path = "../data/models/svm{}.model".format(cont)

    for name, value in parser.parse_args()._get_kwargs():
        if name == "step_size":
            if value is not None:
                ss = value
        if name == "min_wdw_sz":
            if value is not None:
                mws = value
        if name == "threshold":
            if value is not None:
                threshold = value
        if name == "pixels_per_cell":
            if value is not None:
                ppc = value
        if name == "cells_per_block":
            if value is not None:
                cpb = value

    # Load the classifier
    clf = joblib.load(model_path)

    
    if group==1:
        vect=obj1
    else:
        vect=obj2
    for v in vect:
        if v != obj:
            num=0
            print "   {} - HNM con {}".format(obj,v)
            path = "../data/dataset/{}Data/negOr/".format(v)
            for im_path in glob.glob(os.path.join(path, "*")):
                #print im_path
                scale = 0
                # Read the image
                im = cv2.imread(im_path,cv2.IMREAD_COLOR)
                for im_scaled in pyramid(im, d, mws):
                    cd = []
                    # If the width or height of the scaled image is less than
                    # the width or height of the window, then end the iterations.
                    if im_scaled.shape[0] < mws[1] or im_scaled.shape[1] < mws[0]:
                        break
                    for (x, y, im_window) in sliding_window(im_scaled, mws, ss):
                        if im_window.shape[0] != mws[1] or im_window.shape[1] != mws[0]:
                            continue
                        
                        # Calculate the HOG features
                        if custom_hog==False:
                            object=""
                        else:
                            object=obj                         
                        fdd = hog_cv(im_window,ppc,cpb,orientations,object)
                        fd = fdd.reshape(1, -1)
                        pred = clf.predict(fd)
                        if pred == 1:
                            #ora salvo il falso positivo come negativo
                            fd_name = "hnm-{}-{}.feat".format(v,num)
                            fd_path = os.path.join(neg_feat_path, fd_name)
                            joblib.dump(fdd, fd_path)
                            num+=1
                            #print  "Detection:: Location -> ({}, {}) ({}, {})".format(x, y, int(mws[0]*(d**scale)), int(mws[1]*(d**scale)))
                            #print "Scale ->  {} | Confidence Score {} \n".format(scale, clf.decision_function(fd))
                            if visualize_det:
                                cd.append((x, y, clf.decision_function(fd), int(mws[0]*(d**scale)), int(mws[1]*(d**scale)), int(x*(d**scale)), int(y*(d**scale))))

                        # If visualize is set to true, display the working
                        # of the sliding window
                        if visualize_det:
                            clone = im_scaled.copy()
                            for x1, y1, _, _, _, _, _  in cd:
                                # Draw the detections at this scale
                                cv2.rectangle(clone, (x1, y1), (x1 + im_window.shape[1], y1 + im_window.shape[0]), (0, 0, 0), thickness=2)
                            cv2.rectangle(clone, (x, y), (x + im_window.shape[1], y + im_window.shape[0]), (255, 255, 255), thickness=2)
                            cv2.imshow("Sliding Window in Progress", clone)
                            if wait:
                                cv2.waitKey(30)
                    # Move the the next scale
                    scale += 1

   
    
    #cancello il vecchio model
    if cont==0:
        shutil.rmtree(os.path.split(model_path)[0])
    else:
        os.remove(model_path)
    # retraining
    print "re-training {}".format(obj)
    os.system("python ./train-classifier.py -cont {}".format(cont))






