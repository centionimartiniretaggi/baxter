#!/usr/bin/python
import os
import shutil
import glob
from config import *
import threading as t
import datetime


def run(objs,cont):
    print "INIZIO Thread {}: {}".format(cont, datetime.datetime.now())
    if cont!=0:
        posf_path = "../data/features/pos{}".format(cont)
        negf_path = "../data/features/neg{}".format(cont)
        m_path = "../data/models/svm{}.model".format(cont)
    else:
        posf_path = pos_feat_path
        negf_path = neg_feat_path
        m_path = model_path

    for o in objs:
        print "Oggetto da testare --> {}      time={}".format(o,datetime.datetime.now())
        # Extract the features
        print "Inizio extract - {}".format(o)
        pos_path = "../data/dataset/{}Data/pos/".format(o)
        neg_path = "../data/dataset/{}Data/neg/".format(o)
        os.system("python ./extract-features-cv.py -p {} -n {} -obj {} -cont {}".format(pos_path, neg_path, o, cont))

        # Perform training
        print "Inizio train - {}".format(o)
        os.system("python ./train-classifier.py -cont {}".format(cont))

        # Perform hard-negative mining
        print "Inizio hard-negative mining - {}".format(o)
        os.system("python ./hard-negative-mining.py -obj {} -cont {}".format(o, cont))

        # Perform testing
        print "Inizio test - {}".format(o)
        for im_path in glob.glob(os.path.join(test_im_path, "*")):
            #print "   {}".format(im_path)
            os.system("python ./test-classifier-cv.py -i {} -obj {} -cont {}".format(im_path,o,cont))

        #ora devo cancellare le cartelle data/features e data/models
        #if cont==0:
            #shutil.rmtree(os.path.split(posf_path)[0])
            #shutil.rmtree(os.path.split(m_path)[0])
        #else:
            #shutil.rmtree(posf_path)
            #shutil.rmtree(negf_path)
            #os.remove(m_path)

        print " Fine Oggetto testato --> {}      time={}".format(o,datetime.datetime.now())
    print "FINE Thread {}: {}".format(cont, datetime.datetime.now())




obj = raw_input('Insert the object to detect: ')
uobj = obj.title()


if uobj !="All":
    if uobj=="Banana" or uobj=="Box" or uobj=="Orange" or uobj=="Pocket":
        test_im_path = test_im_path1
    else:
        test_im_path = test_im_path2
    run([uobj],0)
    
else:
    if group==1:
        test_im_path = test_im_path1
        thread1 = t.Thread(target=run, args=(["Banana"],1))
        thread2 = t.Thread(target=run, args=(["Box"],2))
        thread3 = t.Thread(target=run, args=(["Orange"],3))
        thread4 = t.Thread(target=run, args=(["Pocket"],4))
    else:
        test_im_path = test_im_path2
        thread1 = t.Thread(target=run, args=(["Book"],1))
        thread2 = t.Thread(target=run, args=(["Bottle"],2))
        thread3 = t.Thread(target=run, args=(["Glasses"],3))
        thread4 = t.Thread(target=run, args=(["Key"],4))

    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()