# README #

Questo progetto è stato realizzato come parte integrante del corso di Misure e Strumentazione per l'Automazione della Laurea Magistrale in Ingegneria Informatica e dell'Automazione presso l'UNIVPM, tenuto dal prof A. Freddi nell' A.A. 2016/2017. Tratta lo studio e l’implementazione dell’object detection con conseguente task motorio (pick and place) mediante l’utilizzo del Baxter Research Robot. L’object detection è supportata da una metodologia, nel campo della computer vision, che consente di riconoscere e trovare oggetti in un’immagine o in un video.
In secondo luogo, il robot dovrà prendere l’oggetto identificato e riposizionarlo su un altro punto: quest’operazione verrà denominata “pick and place”.

Il robot dovrà svolgere le seguenti operazioni:

* Inquadrare un’immagine, ovvero la nostra scena, tramite la webcam.
* Identificare e determinare la posizione di un oggetto dato in input.
* Pick and place: con una delle due braccia a disposizione, posizionarsi sopra l’oggetto desiderato, afferrarlo, e riposizionarlo in un altro punto dello spazio.

Le difficoltà maggiori nello sviluppo di questo lavoro interessano soprattutto la fase dell’object detection: distinguere un oggetto tra molti altri, posto in diverse posizioni, di diverse dimensioni può risultare complesso. Il problema è stato affrontato utilizzando una combinazione di due metodologie: 

* L’algoritmo HOG (Hisogram of Oriented Gradient) che consente l’estrazione di features da un’immagine.
* Il classificatore SVM che, propriamente addestrato, è in grado di rilevare l’oggetto desiderato a partire da una nuova immagine.

![Baxter Robot](https://cdn.arstechnica.net/wp-content/uploads/2014/06/main2.jpg)

## LAVORO IN DETTAGLIO ##

* [Tesina](https://bitbucket.org/centionimartiniretaggi/baxter/raw/7c96be306fbdfb11153a3c9de79e081ecbc611ad/Tesina.pdf)
* [Presentazione](https://bitbucket.org/centionimartiniretaggi/baxter/raw/7c96be306fbdfb11153a3c9de79e081ecbc611ad/Presentazione.ppsx)

## STRUMENTI UTILIZZATI ##

* Baxter Robot
* Ubuntu 14.04
* Python (ver. 2.7.6)
* ROS (Robot Operating System)
* OpenCV (ver. 3.2.0) per la computer vision
* Skimage (ver. 0.13dev) per l’utilizzo dell’algoritmo HOG (libreria poi abbandonata in quanto anche le OpenCV ha delle proprie funzioni per applicare l’HOG)
* Sklearn (ver. 0.18.1) per l’implementazione del classificatore SVM (Support Vector Machine)
* Libreria pybarrier.py per la messa a punto di codice multithread
* Libreria transformations.py per effettuare la rotazione del gripper

## UTILIZZO DEL CODICE ##

Per l’esecuzione del programma di pick and place, è necessario eseguire lo script movement-final.py nella cartella BAXTER e seguire le istruzioni riportate sul terminale, consentendo all’utilizzatore di:

* verificare che le braccia siano nella giusta posizione: premere un tasto per continuare;
* scegliere quale braccio utilizzare;
* scegliere l’oggetto da individuare;
* come forma di sicurezza, prima di procedere per la fase di pick and place, viene visualizzato come l’algoritmo ha individuato l’oggetto, verificare quindi che il centro dell’oggetto sia stato rilevato correttamente: premere un tasto per continuare;

Al termine dell’algoritmo, l’operazione di pick and place sarà conclusa. Il comando per l’esecuzione dello script è il seguente:

* python movement-final.py

Nota: a monte di questo script è già presente il comando per l’untuck.
I file functions.py e testhogsvm.py contengono tutte le funzioni necessarie per l’algoritmo di object detection, mentre I modelli di classificazione utilizzati si trovano nella cartella models. 

La prima versione del progetto in cui si utilizza la libreria skimage è situata nella cartella “HOG+SVM con skimage” e il comando principale per l’esecuzione del codice è:

* python run.py

L’ultima versione per l’object detection, abbandonando la libreria skimage e con classificatore multithread, è nella cartella “HOG+SVM con OPENCV” e il suo comando principale è:

* python run-cv.py

Per l’esecuzione dell’algoritmo che identifica qual è il parametro C migliore è necessario avere prima le feature già estratte tramite l’HOG. Il codice in output creerà anche il relativo model. Lo script si trova nella stessa cartella di run-cv.py e il comando per l’esecuzione di tale algoritmo è il seguente:

* python bestSVM.py

Nella cartella “DATASET DI PARTENZA” sono presenti gli snapshot originali a colori, da cui ne vengono selezionati alcuni per utilizzare uno script chiamato crea-negpos.py per l’utilizzo dell’HOG classico:

* python crea-negpos.py

Nella cartella CUSTOM-HOG, situata in DATASET DI PARTENZA/SELEZIONATI/, c’è lo script crea-negpos-custom.py che crea le immagini positive per poter utilizzare il custom HOG.

* python crea-negpos-custom.py

## REFERENZE ##

* [Baxter](http://www.rethinkrobotics.com/baxter/)
* [HOG + SVM](http://lear.inrialpes.fr/people/triggs/pubs/Dalal-cvpr05.pdf)

## RELATORI ##

* Davide Centioni
* Diego Retaggi
* Massimo Martini

